import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
            
        main main = new main();
        main.geraCustoFechado();
        main.geraCustoAberto();
    }

    public void geraCustoAberto(){
        int i = 50;
        Random rand = new Random();

        for(i = 50; i <= 100; i++) {
            int diaEntrada = rand.nextInt(30) + 1;
            int mesEntrada = rand.nextInt(12) + 1;
            int anoEntrada = rand.nextInt(4) + 2020;

            String dataEntrada = diaEntrada + "/" + mesEntrada + "/" + anoEntrada;

            System.out.println("INSERT INTO ordem_servico (marca, modelo, tipo, problema, status, data_entrada, ativo) VALUES ('marca " + i + "', 'modelo " + i + "', 'tipo " + i + "', 'problema " + i + "', '" + dataEntrada + "', 'aberta', true);");

            System.out.println("");
        }
    }

    public void geraCustoFechado(){
        ArrayList<String> insertCusto = new ArrayList<>();
        ArrayList<Double> valorCusto = new ArrayList<>();

        String insert = "";

        double valor1 = 0;

        int i = 0;
        int x = 0;

        for(i = 1; i <= 50; i++){
            Random rand = new Random();
            int custosQuantidade = rand.nextInt(5) + 1;

            for(int j = 0; j < custosQuantidade; j++){
                rand = new Random();
                int idProduto = rand.nextInt(50) + 1;

                rand = new Random();
                double valor = rand.nextDouble() * 250 + 1;
                valor = Math.round(valor * 100.0) / 100.0;

                valorCusto.add(valor);

                String idProdutoString = String.valueOf(idProduto);
                String valorString =  String.valueOf(valor);
                int idOrdem = i;
                String idOrdemString =  String.valueOf(idOrdem);

                insert = "INSERT INTO custo (id_produto, id_ordemservico, valor) VALUES (" + idProdutoString + ", " + idOrdemString + ", " + valorString + ");";
                insertCusto.add(insert);
                insert = "";
            }

            int diaEntrada = rand.nextInt(30) + 1;
            int mesEntrada = rand.nextInt(12) + 1;
            int anoEntrada = rand.nextInt(4) + 2020;
            int diaSaida = 0;
            int mesSaida = 0;
            int anoSaida = 0;


            while (x == 0){
                diaSaida = rand.nextInt(30) + 1;
                if(diaSaida >= diaEntrada){
                    x = 1;
                }
            }
            x = 0;
            while (x == 0){
                mesSaida = rand.nextInt(12) + 1;
                if(mesSaida >= mesEntrada){
                    x = 1;
                }
            }
            x = 0;
            while (x == 0){
                anoSaida = rand.nextInt(4) + 2020;
                if(anoSaida >= anoEntrada){
                    x = 1;
                }
            }
            x = 0;

            String dataEntrada = diaEntrada + "/" + mesEntrada + "/" + anoEntrada;
            String dataSaida = diaSaida + "/" + mesSaida + "/" + anoSaida;


            for (double v : valorCusto){
                valor1 = valor1 + v;
            }

            System.out.println("INSERT INTO ordem_servico (marca, modelo, tipo, problema, data_entrada, data_saida, total, status, ativo) VALUES ('marca " + i + "', 'modelo " + i + "', 'tipo " + i + "', 'problema " + i + "', '" + dataEntrada + "', '" + dataSaida + "', " + valor1 + ", 'fechada', false);");

            for (String insertc : insertCusto){
                System.out.println(insertc);
            }

            System.out.println("");

            valor1 = 0;
            insertCusto.clear();
            valorCusto.clear();
        }
    }
}