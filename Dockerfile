FROM openjdk:8-jdk-alpine

COPY . /app

WORKDIR /app

RUN javac main.java

CMD ["java", "main"]